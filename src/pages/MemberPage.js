import React from 'react'
// import { UserStoreContext } from '../context/UserContext'
import { useSelector } from 'react-redux'

const MemberPage = () => {
  // const userStore = React.useContext(UserStoreContext)
  const profileRedux = useSelector((state) => state.authReducer.profile)

  return (
    <div className='container mt-4'>
        <div className='row'>
            <div className='col-md-12'>
                <h2>สำหรับสมาชิก</h2>
                {
                  profileRedux && (
                    // <p>ยินดีต้อนรับคุณ {userStore.profile.name} Email: {userStore.profile.email}</p>
                    <p>ยินดีต้อนรับคุณ {profileRedux.name} Email: {profileRedux.email}</p>
                  )
                }
                
            </div>
        </div>
    </div>
  )
}

export default MemberPage