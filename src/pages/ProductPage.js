import React from 'react'
import axios from 'axios'

import {Table, Image, Badge, Spinner } from 'react-bootstrap';
import { format } from 'date-fns'
import {th} from 'date-fns/locale'

import { BsEyeFill } from 'react-icons/bs'
import { Link } from 'react-router-dom'

//redux
import { addToCart } from '../redux/actions/cartAction'
import { useDispatch, useSelector } from 'react-redux'


const ProductPage = () => {
    const [product, setProduct] = React.useState([])
    const [loading, setLoading] = React.useState(false)
    const [error, setError] = React.useState(null)
    // const cancelToken = React.useRef(null)
    const cancelToken = React.useRef(null);

    //redux
    const dispatch = useDispatch()
    const cart = useSelector(state => state.cartReducer.cart)
    const total = useSelector(state => state.cartReducer.total)

    const getData = async () => {
    try {
      setLoading(true);
      const resp = await axios.get(
        "https://api.codingthailand.com/api/course",{
          cancelToken: cancelToken.current.token,
        });
      //console.log(resp.data.data)
      setProduct(resp.data.data);
    } catch (error) {
      console.log(error);
      setError(error);
    } finally {
      setLoading(false);
    }
  };

  React.useEffect(() => {
    cancelToken.current = axios.CancelToken.source();

    getData();

    // return () => {
    //   console.log('exit product page')
    // //   cancelToken.current.cancel();
    // };
  }, []);

  if (loading === true) {
    return (
      <div className="text-center mt-5">
        <Spinner animation="border" variant="primary" />
      </div>
    );
  }

  if (error) {
    return (
      <div className="text-center mt-5 text-danger">
        <p>เกิดข้อผิดพลาดจาก Server กรุณาลองใหม่</p>
        <p>{error.response.data.message}</p>
      </div>
    );
  }

  const addCart = (p) => {
    // console.log(p)
    const product = {
      id: p.id,
      name: p.title,
      price: p.view, //สมมุติ ใช้ view แทนราคา
      qty: 1
    }

    //call action
    dispatch(addToCart(product, cart))

    
  }

  return (
      <div className='container mt-4'>
          <div className='row'>
              <div className='col-md-12'>
                  <h2>
                    สินค้า
                    {
                      total > 0 && <h4>ซื้อแล้ว {total} ชิ้น</h4>
                    }
                  </h2>
                  <Table striped bordered hover>
                      <thead>
                          <tr>
                              <th>id</th>
                              <th>ชื่อคอร์ส</th>
                              <th>รายละเอียด</th>
                              <th>วันที่สร้าง</th>
                              <th>views</th>
                              <th>รูปภาพ</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody>
                          {
                              product.map((p, index) => {
                                  return(
                              <tr>
                                  <td>{p.id}</td>
                                  <td>{p.title}</td>
                                  <td>{p.detail}</td>
                                  <td>
                                      {
                                          format(new Date(p.date), 'dd/MM/yyyy', {locale: th})
                                      }
                                      
                                      </td>
                                  <td><Badge variant="primary">{p.view}</Badge></td>
                                  <td>
                                      <Image src={p.picture} thumbnail alt={p.detail} width={100}/>
                                  </td>
                                  <td>
                                      <Link to={`/detail/${p.id}/title/${p.title}`}>
                                          <BsEyeFill />
                                      </Link>

                                      <button 
                                        className='btn btn-outline-success ml-2'
                                        onClick={() => addCart(p)}
                                      >
                                        หยิบใส่ตะกร้า
                                      </button>
                                  </td>
                              </tr>

                                  )
                              })
                          }

                      </tbody>
                  </Table>
              </div>
          </div>
      </div>
    )
}

export default ProductPage