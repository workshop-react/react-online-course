import React from 'react'
import { PDFViewer, Page, Text, View, Document, StyleSheet, Font, Image } from '@react-pdf/renderer';
import { useSelector } from 'react-redux'
import { Table, TableHeader, TableCell, TableBody, DataTableCell } from '@david.kucsai/react-pdf-table'




// Create styles
const styles = StyleSheet.create({
    page: {
    //   flexDirection: 'row',
    //   backgroundColor: '#E4E4E4',
      fontFamily: "Sarabun",
      paddingTop: 35,
      paddingBottom: 65,
      paddingHorizontal: 35,
    },
    title: {
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 20
    },
    container: {
        alignSelf: 'center',
        marginBottom: 10
    }
    // section: {
    //   margin: 10,
    //   padding: 10,
    //   flexGrow: 1
    // }
  });

Font.register({
    family: "Sarabun",
    fonts: [
        {src: "./fonts/Sarabun-Regular.ttf"},
    ]
})



const PdfReport = () => {
    const cart = useSelector(state => state.cartReducer.cart)

    //หน่วยเวลาเพื่อป้องกัน error
    const [isOnPdfView, setIsOnPdfView] = React.useState(false)

    React.useEffect(() => {
        setTimeout(() => {
            setIsOnPdfView(true)
        }, 1000)
    }, [])


  return (
    <>
    {
        isOnPdfView && (
            <PDFViewer className='container-fluid' height={600}>
                <Document>
                    <Page size="A4" style={styles.page}>
                    <View style={styles.container}>
                        <Image src="./logo192.png" style={{width:50}} />
                    </View>
                    <View style={styles.title}>
                        <Text>รายงานการสั่งซื้อสิ้นค้า</Text>
                    </View>
                    <Table
                        data={cart}
                    >
                        <TableHeader textAlign='center'>
                            <TableCell weighting={0.5}>รหัสสินค้า</TableCell>
                            <TableCell>ชื่อสินค้า</TableCell>
                            <TableCell>ราคา</TableCell>
                            <TableCell>จำนวน </TableCell>
                            <TableCell>รวม</TableCell>
                        </TableHeader>
                        <TableBody>
                            <DataTableCell weighting={0.5} style={{textAlign:'center'}} getContent={(r) => r.id}/>
                            <DataTableCell getContent={(r) => r.name}/>
                            <DataTableCell style={{textAlign:'center'}} getContent={(r) => r.price}/>
                            <DataTableCell style={{textAlign:'center'}} getContent={(r) => r.qty}/>
                            <DataTableCell style={{textAlign:'center'}} getContent={(r) => r.price * r.qty}/>
                        </TableBody>
                    </Table>
                    
                    </Page>
                </Document>
                </PDFViewer>
        )
    }
    </>
    
  )
}

export default PdfReport