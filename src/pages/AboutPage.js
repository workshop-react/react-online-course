/* eslint-disable no-unused-vars */
import React from 'react'
import { Beforeunload } from 'react-beforeunload';

import axios from 'axios'
import JWT from 'expo-jwt';
import { useHistory } from "react-router-dom";


const removeApplicationData = () => {
  if (window) { // NextJS is ServerSideRendering, therefore the window-check.
      localStorage.clear();
  }
};

const AboutPage = () => {
  const [version, setVersion] = React.useState('')
  const [profile, setProfile] = React.useState(null)
  const history = useHistory()
  


  return (
    <div className='container mt-4'>
        <div className='row'>
            <div className='col-md-12'>
                <h2>เกี่ยวกับเรา</h2>
                {
                  version && (
                    <p>
                      Backend API Version {version}
                    </p>
                  )
                }
                <p>
                 NAME
                </p>
                
            </div>
        </div>
    </div>
  )
}

export default AboutPage