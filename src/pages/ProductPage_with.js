import React from 'react'
import axios from 'axios'

import {Table, Image, Badge, Spinner } from 'react-bootstrap';
import { format } from 'date-fns'
import {th} from 'date-fns/locale'

const ProductPage = () => {
    const [product, setProduct] = React.useState([])
    const [loading, setLoading] = React.useState(false)
    const [error, setError] = React.useState(null)
    // const cancelToken = React.useRef(null)
    const cancelToken = React.useRef(null);

    const getData = async () => {
    try {
      setLoading(true);
      const resp = await axios.get(
        "https://api.codingthailand.com/api/course",
        // {
        //   cancelToken: cancelToken.current.token,
        // }
      );
      //console.log(resp.data.data)
      setProduct(resp.data.data);
    } catch (error) {
      console.log(error);
      setError(error);
    } finally {
      setLoading(false);
    }
  };

  React.useEffect(() => {
    cancelToken.current = axios.CancelToken.source();

    getData();

    // return () => {
    //   console.log('exit product page')
    // //   cancelToken.current.cancel();
    // };
  }, []);

  if (loading === true) {
    return (
      <div className="text-center mt-5">
        <Spinner animation="border" variant="primary" />
      </div>
    );
  }

  if (error) {
    return (
      <div className="text-center mt-5 text-danger">
        <p>เกิดข้อผิดพลาดจาก Server กรุณาลองใหม่</p>
        <p>{error.response.data.message}</p>
      </div>
    );
  }

    return (
        <div className='container mt-4'>
            <div className='row'>
                <div className='col-md-12'>
                    <h2>สินค้า</h2>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>ชื่อคอร์ส</th>
                                <th>รายละเอียด</th>
                                <th>วันที่สร้าง</th>
                                <th>views</th>
                                <th>รูปภาพ</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                product.map((p, index) => {
                                    return(
                                        <tr>
                                            <td>{p.id}</td>
                                            <td>{p.title}</td>
                                            <td>{p.detail}</td>
                                            <td>
                                                {
                                                    format(new Date(p.date), 'dd/MM/yyyy', {locale: th})
                                                }
                                                
                                                </td>
                                            <td><Badge variant="primary">{p.view}</Badge></td>
                                            <td>
                                                <Image src={p.picture} thumbnail alt={p.detail} width={100}/>
                                            </td>
                                        </tr>

                                    )
                                })
                            }

                        </tbody>
                    </Table>
                </div>
            </div>
        </div>
      )
}

export default ProductPage