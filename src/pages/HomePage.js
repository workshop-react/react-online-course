import React from 'react'
import { useQuery } from 'react-query'
import { BsFillBriefcaseFill } from "react-icons/bs";
import { Link } from 'react-router-dom'
import { Spinner } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux'
import { updateProfile } from '../redux/actions/authAction';

const HomePage = () => {
  const profileRedux = useSelector((state) => state.authReducer.profile)
  
  // const { isLoading, error, data, isFetching } = useQuery("getData", () =>
  //   fetch(
  //     "https://api.codingthailand.com/api/news?page=1&per_page=3"
  //   ).then((res) => res.json())
  // );

  const query = useQuery("getData", () => {
    const controller = new AbortController()
    const signal = controller.signal

    const promise = fetch(
          "https://api.codingthailand.com/api/news?page=1&per_page=3", {
            method: 'get',
            signal: signal
          }
        ).then((res) => res.json())

    //cancel request
    promise.cancel = () => controller.abort()

    return promise
  })
   
  const { isLoading, error, data, isFetching } = query


  // console.log(data.data)

  if (isLoading === true) {
    return (
      <div className="text-center mt-5">
        <Spinner animation="border" variant="primary" />
      </div>
    );
  }

  if (error) {
    return (
      <div className="text-center mt-5 text-danger">
        <p>เกิดข้อผิดพลาดจาก Server กรุณาลองใหม่</p>
        <p>{error.response.data.message}</p>
      </div>
    );
  }

  return (
    <>

    </>
  )
}

export default HomePage