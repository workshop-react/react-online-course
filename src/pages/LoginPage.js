/* eslint-disable no-unused-vars */
import React from 'react'
import { Container,Form, Button, Row, Col  } from 'react-bootstrap';
import { useToasts } from 'react-toast-notifications';
import JWT from 'expo-jwt';
// import { UserStoreContext } from '../context/UserContext'

//redux
import { useDispatch } from 'react-redux'


import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";

import axios from 'axios';

import { useHistory } from 'react-router-dom'
import { updateProfile } from '../redux/actions/authAction';

const schema = yup.object({
  usr: yup.string().required('อีเมลล์ห้ามว่าง'),
  password: yup.string().required('รหัสผ่านห้ามว่าง').min(3, 'รหัสผ่านต้อง 3 ตัวอักษรขึ้นไป'),
}).required();

const LoginPage = () => {
  const history = useHistory()
  const { addToast } = useToasts();
  // const userStore = React.useContext(UserStoreContext)


  //call redux action
  const dispatch = useDispatch()

  const { register, handleSubmit, formState:{ errors } } = useForm({
    resolver: yupResolver(schema)
  });
  
  const onSubmit = async (data) => {
    // console.log(data)
    
    try {
        // const apiUrl = "https://api.codingthailand.com/api/login";
        let apiUrl = "http://localhost:5000/api/v1/auth/login";
        let resp = await axios.post(apiUrl, {
            usr: data.usr,
            password: data.password,
        });
        
        console.log(resp.data.access_token)
        localStorage.setItem('token', JSON.stringify(resp.data.access_token))
        // console.log(resp.data.access_token)
        //get profile
        apiUrl = "http://localhost:5000/api/v1/auth/profile"
        const respProfile = await axios.get(apiUrl, {
          headers: {
                    Authorization: `Bearer ${resp.data.access_token}`
                }
        });

        const encodeJWT = JWT.encode(respProfile.data, process.env.REACT_APP_JWT_SECRET);
        // console.log(encodeJWT);
        
        const arrProflie = {
          id: respProfile.data.id,
          name: respProfile.data.name,
          permission: respProfile.data.permission,
          empcode: respProfile.data.usr,
          department: respProfile.data.department.department
        }

        localStorage.setItem('profile', JSON.stringify(arrProflie))

        //บันทึกเรียบร้อย
        addToast('เข้าระบบเรียบร้อยแล้ว', { appearance: "success" });
        

        //update profile by context
        // const profileValue = JSON.parse(localStorage.getItem('profile'))
        // userStore.updateProfile(profileValue) //Context
        // console.log(respProfile.data)
        //call redux
        dispatch(updateProfile(respProfile.data))


        history.replace("/")
        
    } catch (error) {
        addToast(error.response.data.message, { appearance: "error" });
    }
  }

  return (
    <Container className='mt-4'>
        <Row>
          <Col xs={12} md={8}>
          <Form onSubmit={handleSubmit(onSubmit)}>

            <Form.Group controlId="usr">
              <Form.Label>user</Form.Label>
              <Form.Control type="text" name="usr" {...register("usr")} className={`form.Control ${errors.usr ? 'is-invalid' : ''}`}/>
              {
                errors.usr && (
                  <Form.Control.Feedback type="invalid">
                    {errors.usr.message}
                  </Form.Control.Feedback>
                )
              }
            </Form.Group>


            <Form.Group controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" name="password" {...register("password")} className={`form.Control ${errors.password ? 'is-invalid' : ''}`}/>
              {
                errors.password && (
                  <Form.Control.Feedback type="invalid">
                    {errors.password.message}
                  </Form.Control.Feedback>
                )
              }
            </Form.Group>





            <Button variant="primary" type="submit">
              เข้าระบบ
            </Button>
          </Form>

          <hr />
          </Col>
        </Row>

    </Container>
  )
}

export default LoginPage