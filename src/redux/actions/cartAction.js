//หยิบ และ บวก จำนวน ทั้งหมด

export const ADD_TO_CART = 'ADD_TO_CART'
export const CLEAR_ALL_CART = "CLEAR_ALL_CART"

export const addToCart = (product = {}, cart = []) => {
    //product = {} == user ส่งเข้ามา 

    let exists = false //สินค้าเคยถูกเพิ่มหรือยัง ถ้ายังให้ push ถ้ามี ให้บวก

    if (cart.length > 0) {
        for (const c of cart){
            if (c.id === product.id) {
                c.qty++
                exists = true
            }
        }
    }

    if (!exists) {
        cart.push(product)
    }

    //หาผลรวม โดยใช้ .reduce 
    const total = cart.reduce((totalQty, product) => totalQty + product.qty, 0 )

    return {
        type: ADD_TO_CART,
        payload: {
            cart: cart,
            total: total
        }
    }

}

export const clearAllCart = () => {
    const cart = []
    const total = 0

    return {
        type: CLEAR_ALL_CART,
        payload: {
            cart: cart,
            total: total
        }
    }
}