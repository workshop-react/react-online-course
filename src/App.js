/* eslint-disable no-unused-vars */
import React from "react"
import { QueryClient, QueryClientProvider} from 'react-query'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import UserStoreProvider from './context/UserContext'

import { ToastProvider } from 'react-toast-notifications';

import Footer from "./components/Footer"
import NavBar from "./components/NavBar"
import AboutPage from "./pages/AboutPage"
import HomePage from "./pages/HomePage"
import ProductPage from "./pages/ProductPage"
import DetailPage from "./pages/DetailPage"
import HospitalPage from './pages/hospital/HospitalPage';
import IndexPage from "./pages/category/IndexPage"
import CreatePage from "./pages/category/CreatePage"
import EditPage from "./pages/category/EditPage"
import UploadPage from "./pages/UploadPage"
import RegisterPage from "./pages/RegisterPage";
import LoginPage from "./pages/LoginPage";
import MemberPage from "./pages/MemberPage";
import PrivateRoute from "./guard/auth";


//redux setup
import { Provider } from 'react-redux'
import { createStore } from 'redux';
import rootReducer from "./redux/reducers";
import CartPage from "./pages/CartPage";
import configureStore from './redux/configureStore'
import PdfReport from "./pages/report/PdfReport";
import ChartReport from "./pages/report/ChartReport";

const queryClient = new QueryClient()


// const store = createStore(rootReducer) // ของเดิมที่ไม่ใช้ 

const { store } =configureStore()


function App() {
  return (
    <Provider store={store}>
      <UserStoreProvider>
      <ToastProvider 
        placement="top-center"
        autoDismiss
        autoDismissTimeout={3000}
      >
      <QueryClientProvider client={queryClient}>
        <Router>
          <NavBar />

          <Switch>
              <PrivateRoute exact path="/">
                <HomePage />
              </PrivateRoute>
              <PrivateRoute path="/about">
                <AboutPage />
              </PrivateRoute>

              <PrivateRoute path="/product">
                <ProductPage />
              </PrivateRoute>

              <PrivateRoute path="/detail/:id/title/:title">
                <DetailPage />
              </PrivateRoute>

              <PrivateRoute path="/hospital">
                <HospitalPage />
              </PrivateRoute>

              <PrivateRoute path="/upload">
                <UploadPage />
              </PrivateRoute>

              <PrivateRoute path="/member">
                <MemberPage />
              </PrivateRoute>

              <PrivateRoute path="/register">
                <RegisterPage />
              </PrivateRoute>

              <Route path="/login">
                <LoginPage />
              </Route>

              <PrivateRoute path="/cart">
                <CartPage />
              </PrivateRoute>

              <PrivateRoute path="/pdf">
                <PdfReport />
              </PrivateRoute>
              <PrivateRoute path="/chart">
                <ChartReport />
              </PrivateRoute>

              <PrivateRoute
                  path="/category"
                  render={({ match: { url } }) => (
                    <>
                      <Route path={`${url}/`} exact>
                        <IndexPage />
                      </Route>
                      <Route path={`${url}/create`}>
                        <CreatePage />
                      </Route>
                      <Route path={`${url}/edit/:id`}>
                        <EditPage />
                      </Route>
                    </>
                  )}
                ></PrivateRoute>
          </Switch>
          
          <Footer />
        </Router>
      </QueryClientProvider>
      </ToastProvider>
      </UserStoreProvider>
    </Provider>
  );
}

export default App;
