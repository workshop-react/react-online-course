import React from 'react'
import { logo, title } from '../styles/style'


const Logo = () => {

  const logoImage = {
      url: './logo192.png'
  }

  return (
    <div>
        <h3 style={title}>LOGO</h3>
       
        <img style={logo} src={logoImage.url} width="100" alt="logo"></img>
    </div>
  )
}

export default Logo