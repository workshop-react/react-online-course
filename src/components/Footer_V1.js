import React from 'react'
import PropTypes from 'prop-types';

const Footer = (props) => {
  const {title, website, postcode} = props
  return (
    <>
        <h1>{title} {new Date().getFullYear()}</h1>
        <p style={{color: 'green', fontSize: 30}}>{website} {postcode}</p>
    </>
  )
}

Footer.propTypes = {
  title: PropTypes.string,
  website: PropTypes.string,
  postcode: PropTypes.number,
};



export default Footer