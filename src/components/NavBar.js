/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, {useState} from 'react'
import axios from 'axios';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
// import { UserStoreContext } from '../context/UserContext'

import { AiFillHome } from "react-icons/ai";

import { NavLink, useHistory } from "react-router-dom";

import { useSelector, useDispatch } from 'react-redux'
import { updateProfile } from '../redux/actions/authAction';
// import { cartReducer } from './../redux/reducers/cartReducer';
import JWT from 'expo-jwt';
import async from './../guard/auth';

const NavBar = () => {
    const history = useHistory()
    const [profile, setProfile] = useState(null)
    // const userStore = React.useContext(UserStoreContext)

    //redux
    const profileRedux = useSelector((state) => state.authReducer.profile)
    const total = useSelector((state) => state.cartReducer.total)
    const dispatch = useDispatch()

    const getProfile = async () => {
        const token = JSON.parse(localStorage.getItem('token'))
        const respProfile = await axios.get("http://localhost:5000/api/v1/auth/profile", {
          headers: {
                    Authorization: `Bearer ${token}`
                }
        });

        if(respProfile){
            dispatch(updateProfile(respProfile.data))
        }else{
            localStorage.removeItem('token')
            localStorage.removeItem('profile')
            history.replace('/')
            // history.go(0)
            // userStore.updateProfile(null)
            dispatch(updateProfile(null))
        }
        // console.log(respProfile.data)
        // setProfile(respProfile.data)

      }

    // const getProfile = async () => {
    //     const profileValue = JSON.parse(localStorage.getItem('profile'))
    //     const token = JSON.parse(localStorage.getItem('token'))
    //     const apiUrl = "http://localhost:5000/api/v1/auth/profile"
    //     const respProfile = await axios.get(apiUrl, {
    //       headers: {
    //                 Authorization: `Bearer ${token}`
    //             }
    //     });
        

    //     if (profileValue) {
    //         setProfile(respProfile)
    //         console.log(profileValue)
    //     }else{
    //         localStorage.removeItem('token')
    //         localStorage.removeItem('profile')
    //         history.replace('/')
    //         dispatch(updateProfile(null))
    //     }
    // }
    //context
    // const getProfile = () => {
    //     const profileValue = JSON.parse(localStorage.getItem('profile'))

    //     if (profileValue) {
    //         userStore.updateProfile(profileValue)
    //     }
    // }

    //redux


    React.useEffect(() => {
        console.log('use effect navbar')
        getProfile()
    },[])
    

    const logout = () => {
        localStorage.removeItem('token')
        localStorage.removeItem('profile')
        history.replace('/')
        // history.go(0)
        // userStore.updateProfile(null)
        dispatch(updateProfile(null))
        setProfile(null)
    }

    return (

            <Navbar bg="dark" expand="lg" variant='dark'>
                    <NavLink className="navbar-brand" to="/" exact>
                        <img
                        src="./logo192.png"
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                        alt="CodingThailand Logo"
                        />{" "}
                        Eveandboy
                    </NavLink>

                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                    {
                        profileRedux ? (
                            <Nav className="mr-auto">
                        
                                <NavLink className="nav-link" to="/" exact activeClassName='active'>
                                    <AiFillHome />หน้าหลัก
                                </NavLink>

                                <NavLink className="nav-link" to="/product" activeClassName='active'>
                                    สินค้า
                                </NavLink>

                                <NavLink className="nav-link" to="/cart" activeClassName='active'>
                                    ตะกร้าสินค้า {total} ชิ้น
                                </NavLink>

                                <NavLink className="nav-link" to="/about" activeClassName='active'>
                                    เกี่ยวกับเรา
                                </NavLink>

                                <NavDropdown title="Workshop (Pagination)" id="basic-nav-dropdown">
                                    <NavDropdown.Item onClick={() => {
                                        history.replace('/hospital')

                                    }}>
                                        ข้อมูลสถานพยาบาล
                                    </NavDropdown.Item>

                                    <NavDropdown.Item onClick={() => {
                                        history.replace('/category')

                                    }}>
                                        หมวดหมู่ข่าว (CRUD)
                                    </NavDropdown.Item>
                                </NavDropdown>

                                <NavLink className="nav-link" to="/upload" activeClassName='active'>
                                    อัปโหลด File
                                </NavLink>


                                <NavLink className="nav-link" to="/member" activeClassName='active'>
                                    เมนูสมาชิก 
                                </NavLink>

                                <NavLink className="nav-link" to="/chart" activeClassName='active'>
                                    Chart 
                                </NavLink>
                            </Nav>
                        ) : ""
                    }
                    

                   {/* {
                    userStore.profile ? (
                        <span className="navbar-text text-white">
                            ยินดีต้อนรับคุณ {userStore.profile.name} role: {userStore.profile.role}
                            <button className="btn btn-danger ml-2" onClick={logout}>
                                Log out
                            </button>
                        </span>
                    ) : (
                        <>
                        <Nav>
                            <NavLink className="nav-link" to="/register" activeClassName='active'>
                                สมัครสมาชิก 
                            </NavLink>

                            <NavLink className="nav-link" to="/login" activeClassName='active'>
                                เข้าระบบ
                            </NavLink>
                        </Nav>
                        </>
                    )} */}

                    {
                    profileRedux ? (
                        <span className="navbar-text text-white">
                            ยินดีต้อนรับคุณ {profileRedux.name} role: {profileRedux.permission}
                            <button className="btn btn-danger ml-2" onClick={logout}>
                                Log out
                            </button>
                        </span>
                    ) : (
                        <>
                        <Nav>
                            <NavLink className="nav-link" to="/login" activeClassName='active'>
                                เข้าระบบ
                            </NavLink>
                        </Nav>
                        </>
                    )}
                    </Navbar.Collapse>
                {/* </Container> */}
            </Navbar>

    )
}

export default NavBar