import React from 'react'
import Logo from './Logo'
import Title from '../styles/title/Title'
import { Button } from '../styles/button/Button'

const Header = () => {
    let companyName = "EB"
    const companyAddress = <p>กรุงเทพมหานคร</p>

    let num = 10;

    const showMessage = () => {
        return companyName + '.com'
    }

    const isLogin = true;

    const showMe = () => {
        alert('Hello React');
    }

    const products = [
        {id: 1, name: 'Coke'},
        {id: 2, name: 'Pepsi'}
    ]

  return (
    <>
        <Title>Hello React</Title>
        <h1>บริษัท {companyName}</h1>
        {companyAddress}
        {num + 100} <br />
        {showMessage()}
        { isLogin && <p>ยินดีต้อนรับ</p> }
        {
            isLogin ? <Logo /> : <p>ม่มีสิทธิดู logo</p>
        }
        <br />
        {/* <button onClick={showMe}>Click Me!</button> */}
        <Button primary onClick={showMe}>Click Me!</Button>

        <ul>
        {
            products.map((product, index) => {
                return (
                    <li key={product.id}>{product.name}</li>
                )
            })
        }
        </ul>
        <hr />
        
    </>
  )
}

export default Header