import React from 'react'

const Sidebar = () => {
    // let fullname = 'John'
    const [fullname, setFullname] = React.useState('John')
    const [count, setCount] = React.useState(1)
    const [isShow, setIsShow] = React.useState(true)

    const changeName = () => {
        setFullname('Mary')
        setIsShow(!isShow)
    }

    const addCount = () => {
        let a = count +1
        setCount(a)
    }

    React.useEffect(() => {
        console.log('sidebar useEffect')
    })

    React.useEffect(() => {
        console.log('sidebar useEffect one time only')
    },[])

    React.useEffect(() => {
        console.log('sidebar useEffect => ' + fullname)
    },[fullname])



  return (
    <>
        <h3>Sidebar</h3>
        {
            isShow ? <p>Hello</p> : <p>World</p>
        }
        <p>
            สวัสดี : {fullname}
        </p>
        <button onClick={changeName}>เปลี่ยนชื่อ</button>

        <p>
            จำนวนนับ : {count}
        </p>
        <button onClick={addCount}>+</button>
    </>
  )
}

export default Sidebar