import Footer from './components/Footer';
import Header from './components/Header';
import Logo from './components/Logo';
import './App.css';
import Sidebar from './components/Sidebar';



function App() {
  return (
    <div className='logo'>
      <Logo />

      <Header />

      <Footer title="google" website = 'www.ttt.com' postcode={10110} />

      <hr />

      <Sidebar />
      <hr />


    </div>
  );
}

export default App;
